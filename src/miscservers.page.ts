export const layout = "layouts/layout.tsx";

export default function* () {
	const characters = ["Kaguya", "Mio", "Haruhi", "Kosaki", "Jeanne"];
	for (const character of characters) {
		yield {
			url: `/${character.toLowerCase()}/`,
			character,
			metas: {
				title: `${character} - Landing Page`,
				site: "Server Landing Page",
				description: "A landing page for one of my many random servers.",
				icon: `https://${character.toLowerCase()}.froth.zone/${character.toLowerCase()}_icon.png`,
				image: `https://${character.toLowerCase()}.froth.zone/${character.toLowerCase()}.jpg`,
				robots: true,
				generator: true,
				twitter: "@weezerfan94",
				color: "#000000",
			},
		};
	}
}