export const layout = "layouts/layout.tsx";

export default function* () {
	const characters = ["Konata"];
	for (const character of characters) {
		yield {
			url: `/${character.toLowerCase()}/`,
			character,
			metas: {
				title: `${character} AKA Greg`,
				site: "Greg Landing Page",
				description: "There's a Greg here.",
				icon: `https://${character.toLowerCase()}.froth.zone/${character.toLowerCase()}_icon.png`,
				image: `https://${character.toLowerCase()}.froth.zone/${character.toLowerCase()}.jpg`,
				robots: true,
				generator: true,
				color: "#000000",
			},
		};
	}
}