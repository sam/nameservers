import lume from "lume/mod.ts";
import base_path from "lume/plugins/base_path.ts";
import inline from "lume/plugins/inline.ts";
import jsx_preact from "lume/plugins/jsx_preact.ts";
import lightningCss from "lume/plugins/lightningcss.ts";
import metas from "lume/plugins/metas.ts";
import minify_html from "lume/plugins/minify_html.ts";
import og_images from "lume/plugins/og_images.ts";
import robots from "lume/plugins/robots.ts";

const site = lume({
	emptyDest: true,
	src: "src",	
	dest: "dist",
});

site.use(base_path());
site.use(inline());
site.use(lightningCss())
site.use(jsx_preact());
site.use(metas());
site.use(minify_html());
site.use(og_images());
site.use(robots());

site.copy([".png", ".jpg"], (file) => `${file.replace("/img", "").split(".")[0].split("_icon")[0]}${file.replace("/img", "")}`);
site.copy(".domains", "jeanne/.domains")

export default site;
