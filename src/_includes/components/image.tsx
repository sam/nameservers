export default (o: { char: string }) => (
  <img alt={o.char} title={o.char} src={`${o.char.toLowerCase()}.jpg`} />
);
