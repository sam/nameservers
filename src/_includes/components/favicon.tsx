export default (o: { char: string }) => (
  <link rel="icon" href={`${o.char.toLowerCase()}_icon.png`} />
);
