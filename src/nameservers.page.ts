export const layout = "layouts/layout.tsx";

export default function* () {
	const characters = ["Rin", "Sakura", "Saber", "Illya"];
	for (const character of characters) {
		yield {
			url: `/${character.toLowerCase()}/`,
			character,
			metas: {
				title: `${character} - Froth.zone Nameserver`,
				site: "Nameserver Landing Page",
				description: "A landing page for the froth.zone nameservers",
				icon: `https://${character.toLowerCase()}.froth.zone/${character.toLowerCase()}_icon.png`,
				image: `https://${character.toLowerCase()}.froth.zone/${character.toLowerCase()}.jpg`,
				robots: true,
				generator: true,
				twitter: "@weezerfan94",
				color: "#000000",
			},
		};
	}
}
