import Favicon from "../components/favicon.tsx";
import Image from "../components/image.tsx";

interface OurData extends Lume.Data {
  character: string;
}

export default (data: OurData, helpers: Lume.Helpers) => (
  <html lang="en">
    <head>
      <meta charSet="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <title>{data.metas?.title}</title>
      <Favicon char={data.character} />
      {/* @ts-expect-error */} 
      <link rel="stylesheet" href="/styles.css" inline />
    </head>
    <body>
      <Image char={data.character} />
    </body>
  </html>
);
