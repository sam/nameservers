# Server Template Page

This is the template I use for the landing page on my numerous servers.

It is build with [Lume](https://lume.land) and deployed manually.

All it does is make basic pages with a favicon, a main image along with corresponding meta tags.